FROM node:16.5.0-alpine3.11

WORKDIR /app/

COPY app/package.json ./

RUN apk update && \
    apk upgrade && \
    yarn install && \
    yarn global add @nestjs/cli

COPY app/ ./

CMD [ "yarn", "start:dev" ]