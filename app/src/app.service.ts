import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {

  title: string = 'Hello World!';

  getTitle(): string {
    return this.title;
  }

  putTitle(title: string){
    this.title = title;
  }
}
