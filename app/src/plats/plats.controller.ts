import { Controller, Get, Param, Res, HttpStatus } from '@nestjs/common';
import { Response} from 'express';
@Controller()
export class PlatsController {

    @Get(':id')
    getPlatById(@Param('id') id: string): string {
        return 'Plat' + id;
    }

    @Get()
    findAll(@Res() res: Response) {
        const plats = [];
        
        res.status(HttpStatus.OK).json(plats);
    }
}
