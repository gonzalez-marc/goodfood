import { Controller, Get, Put } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getTitle();
  }

  @Put()
  setTitle(entry: string){
    this.appService.putTitle(entry);
  }
}
