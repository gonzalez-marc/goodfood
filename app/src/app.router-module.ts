import { Routes } from '@nestjs/core';
import { PlatsModule } from './plats/plats.module';

export const routes: Routes = [
  {
    path: 'plats',
    module: PlatsModule,
  },
];
