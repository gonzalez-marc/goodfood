import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PlatsModule } from './plats/plats.module';
import { routes } from './app.router-module';

@Module({
  imports: [RouterModule.register(routes), PlatsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
